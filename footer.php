    <footer>
        <div class="wrapper">
        
            <div class="links">
                <a id="website-link" class="red" href="http://mdlink.de">zur Webseite</a> -
                <a id="impressum-link" class="red" href="/impressum">Impressum</a>
            </div>
            <div id="footer-logo-box">
                <img src="images/mdlink-logo.png" alt="MDlink Logo" />
            </div>
        </div>
    </footer>
  </body>
</html>