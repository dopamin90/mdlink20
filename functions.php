<?php

    require_once('DataProvider.php');

    function get_header($header_name = ''){
        if($header_name != ''){
            require('header-' . $header_name . '.php');
        }else{
            require('header.php');
        }
    }
    
    function get_footer($footer_name = ''){
        if($footer_name != ''){
            require('footer-' . $footer_name . '.php');
        }else{
            require('footer.php');
        }
    }
    
    function load_page($name){
        
        $name = basename($name, '.php');
        $file = 'page-' . $name . '.php';

        if(file_exists($file)){
            require($file);
        }else{
            require('404.php');
        }
        
    }

    function processForm() {
        $result = array('errors'=>array());

        if(!isset($_POST['name']) || !$_POST['name']) {
            $result['errors']['name'] = 'Der Name ist eine Pflichtangabe.';
        }
        if(!isset($_POST['mail']) || !$_POST['mail']) {
            $result['errors']['mail'] = 'Es muss eine gültige Email angegeben werden.';
        }

        $contactName = trim(filter_var($_POST['name'], FILTER_SANITIZE_STRING));
        $contactMail = filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL);

        if(empty($result['errors'])) {
            if($contactName == false) {
                $result['errors']['name'] = 'Der Name ist eine Pflichtangabe.';
            }
            if($contactMail === false) {
                $result['errors']['mail'] = 'Es muss eine gültige Email angegeben werden.';
            } 
        }

        if (!isset($_POST['vortrag'])) {
            $result['errors']['checkboxes'] = 'Sollten Sie teilnehmen wollen, so wählen Sie bitte aus, dass Sie zu den Vorträgen kommen werden.';
        }

        if(!empty($result['errors'])) {
            return $result;
        }


        $dinnerTxt =  "Kommt nicht zum Abendprogramm.";
        if (isset($_POST['dinner'])) {
            $dinnerTxt = "Kommt zum Abendprogramm.";
        }

        $empfaenger = 'kontakt@wamd.de, bozem@wamd.de';
        $betreff = 'MDlink - 20 years for you: Neue Registrierung';
        $nachricht = 'Es gibt eine neue Registrierung: ' . "\r\n" .
                "Name: " . $contactName . "\r\n" .
                "Email: " . $contactMail . "\r\n" .
                $dinnerTxt;

        $header = 'From: kontakt@wamd.de';
        //mail($empfaenger, $betreff, $nachricht, $header);    

        $result['status'] = 'success';
        return $result;    
    }    

?>