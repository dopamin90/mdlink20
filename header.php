<?php header('Content-Type: text/html; charset=utf-8');?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    

    <title>MDlink | &bdquo;20 YEARS FOR YOU&rdquo;</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/style.css">
    <link rel="icon" type="image/x-icon" href="images/favico-mdlink.ico" />
     
    
  </head>
  <body>
      <header>
        
            <div class="wrapper">
            
                <div id="main-logo-box">
                    <a href="http://besonders.mdlink.de"><img src="images/mdlink-logo.png" alt="MDlink Logo" /></a>
                </div>
                
                <div id="slogan-box">
                    <h1><span class="we-celebrate">Wir feiern</span><span class="slogan">&bdquo;20 years for you&rdquo;</span></h1>
                    <h2>25.09.2014</h2>
                </div>
                
            
            </div>
        
        </header>