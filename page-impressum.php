<?php get_header();?>
    
    <div class="wrapper">
        <div id="content">
            
            
            <div id="impressum">
            
            <h1>Impressum</h1><h2>Inhaltliche Verantwortung</h2>
<p><strong>MDlink online service center GmbH</strong><br />Lorenzweg 42<br />39124 Magdeburg</p>
<p>Telefon: +49-391-25568-0<br />Telefax: +49-391-25568-99</p>
<p>E-Mail: <a title="Email an uns schreiben" href="mailto:info@mdlink.de"><script type="text/javascript">/* <![CDATA[ */document.write("<n uers=\"znvygb:vasb@zqyvax.qr\">vasb@zqyvax.qr</n>".replace(/[a-zA-Z]/g, function(c){return String.fromCharCode((c<="Z"?90:122)>=(c=c.charCodeAt(0)+13)?c:c-26);}));/* ]]> */</script><noscript><em>&gt;&gt;&gt; Bitte JavaScript aktivieren um die Email-Adresse sichtbar zu machen! &lt;&lt;&lt;</em></noscript><br /></a>Internet: <a href="http://www.mdlink.de" target="_blank">www.mdlink.de</a></p>
<p>Geschäftsführer: Olaf Müller<br />Handelsregister: HRB 106650, Registergericht: Amtsgericht Stendal, Firmensitz: Magdeburg</p>
<p>Umsatzsteuer-ID: DE163829671</p>
<h2>Konzeption und Realisierung</h2>
<p>
    webakademie UG (haftungsbeschränkt)<br>
    <a href="http://wamd.de">www.wamd.de</a><br>
    
</p>	
            
            </div><!-- datebox !-->
        </div>
    </div>
               
<?php get_footer();?>
