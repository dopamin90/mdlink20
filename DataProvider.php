<?php

class DataProvider {

	const DATA_FILE = 'data/data.json';

	private static $data = null;
	private static $calendar = null;
	public static $calCategories = array('empfang', 'vortrag', 'abendprogramm');

	public static function loadHistory() {
		return self::loadData()->history;
	}

	public static function getCategoryName($cat) {
		self::initCalendar();

		if ($cat === self::$calCategories[0])
			return "Empfang";
		elseif ($cat === self::$calCategories[1])
			return "Vorträge";
		else 
			return "Abendprogramm";
	}

	public static function loadCalendar($category) {
		self::initCalendar();
		return self::$calendar[$category];
	}

	private static function initCalendar() {
        if (self::$calendar === null) {
			$entries = self::loadData();
			$groupedEntries = array($calCategories[0]=>array(), $calCategories[1]=>array(), $calCategories[2]=>array());
			foreach ($entries->calendar as $entry) {
				$groupedEntries[$entry->category][] = $entry;
			}

			/*
			foreach ($groupedEntries as $key => $value) {
				usort($groupedEntries[$key], function($a, $b) {return $a->time - $b->time;});
			}
			*/

			self::$calendar = $groupedEntries;
        }
	}

	private static function loadData() {
        if (self::$data === null) {
            self::$data = json_decode(file_get_contents(self::DATA_FILE));
        }
        
        return self::$data;
    }
}