<?php

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	require_once('functions.php');

    $formResult = processForm();

    if(!empty($formResult['errors'])) {
    	//http_response_code(400); //works with php version >= 5.4
    	header('X-PHP-Response-Code: 400', true, 400);
    } else {
    	unset($formResult['errors']);
    }

    echo json_encode($formResult);
}