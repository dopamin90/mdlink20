<?php get_header();?>
    
    <div class="wrapper">
        <div id="content">
            <div id="threeslide-box">
                <div class="mask" id="mask-1">
                    
                    <img src="images/1994.jpg" class="img-placeholder">
                    <div class="black-layer">
                        <h3>Unsere Geschichte<br/>&bdquo;20 YEARS FOR YOU&rdquo;</h3>
                        <a class="scroll-link" href="#history">Erfahen Sie mehr<br/>über unsere Geschichte</a>
                    </div>
                    
                </div>
                
                <div class="mask" id="mask-2">
                    
                    <img src="images/program.jpg" class="img-placeholder">
                    <div class="black-layer">
                        <h3>Wir feiern mit<br />Ihnen mit großem<br />Programm</h3>
                        <a class="scroll-link" href="#programm">Erfahren Sie mehr über<br />Vorträge und unsere Abendveranstaltung</a>
                    </div>
                    
                </div>
                
                <div class="mask" id="mask-3">
                    
                    <img src="images/italian-food.jpg" class="img-placeholder">
                    <div class="black-layer">
                        <h3>Jetzt zusagen!<br />70 kostenlose<br />Plätze für Gäste</h3>
                        <a class="scroll-link" href="#zusagen">Melden Sie sich jetzt<br />für die Veranstaltung an</a>
                    </div>
                    
                </div>
            
            </div><!-- theeslide !-->
            
            <div id="datebox">
            
                <div id="callendar-sign">
                    <i class="fa fa-calendar"></i>
                </div>
                <div class="information">
                    <p>
                    25.09.2014<br>
                    in der FestungMark Magdeburg
                    </p>
                    <a href="#">Hohepfortewall 1 - 39104 Magdeburg</a> - 
                    <a href="data/mdlink.ics" class="red">Termin als .ics-Datei herunterladen</a>
                </div>
            
                <div class="supported-by">
                    <img src="images/ibm-logo.jpg" alt="IBM" class="ibm-logo" />
                    <p>mit freundlicher <br>Unterstützung von IBM</p>
                </div>
            
            </div><!-- datebox !-->
            
            <div class="history-map">
                
                <div class="box"  id="history">
                    <h2>Meilensteine von 1994 bis 2014</h2>
                    <p class="center">MDlink wird 20 Jahre und das vor allen Dingen dank Ihnen!<br>
Eine Geschichte von über 20 Jahren und in mehr als 5 Städten</p>

                    <?php foreach(DataProvider::loadHistory() as $history):?>
                        <h3 class="year"><?php echo $history->year;?></h3>
                        <div class="fader">
                            <h4 class="event"><?php echo $history->header;?></h4>

                            <?php if(isset($history->img_path)):?>
                                <img src="<?php echo $history->img_path;?>" alt="Bildbezeichnung" class="<?php echo (isset($history->img_class)) ? $history->img_class : '';?>"/>
                            <?php endif;?>

                            <?php if(isset($history->text_header)):?>
                                <h5 class="textheadline"><?php echo $history->text_header;?></h5>
                            <?php endif;?>

                            <?php if(isset($history->text)):?>
                                <p><?php echo $history->text;?></p>
                            <?php endif;?> 

                            <?php if(isset($history->date) || isset($history->location)):?>
                                <div class="date-place">
                                    <?php if(isset($history->date)):?>
                                        <i class="fa fa-calendar fa-2x"></i> <?php echo $history->date;?> 
                                    <?php endif;?>
                                    <?php if(isset($history->date) && isset($history->location)):?>
                                        -  
                                    <?php endif;?>
                                    <?php if(isset($history->location)):?>
                                        <i class="fa fa-map-marker fa-2x"></i> <?php echo $history->location;?>
                                    <?php endif;?>
                                </div>
                            <?php endif;?>
                        </div>                    
                    <?php endforeach;?>
                </div>
                <div class="box">
                    <h2>Anfahrt</h2>
                    
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2448.8222928286523!2d11.646286!3d52.137554!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbfd7dbb8377776a4!2sFestungMark+Betriebsgesellschaft+mbH!5e0!3m2!1sde!2sde!4v1405419913729"  frameborder="0" style="border:0; width:100%; height:300px;"></iframe>
                    <h3>FestungMark Magdeburg</h3>
                    <address>
                        Hohepfortewall 1<br>
                        39104 Mageburg
                    </address>
                    
                </div>
            
            </div>
            
            <div class="agenda-invite">
            
                <div class="box agenda"   id="programm">
                    <h2>Agenda und Vorträge</h2>
                    <p class="center">
                        Wir feiern Sie, unsere treuen Kunden und uns am 25.09.2014 in gebührenden Rahmen in der Festung Mark. In einem zweiteiligen Prgramm können Sie zuerst interessanten Fach-Vorträgen von uns und einigen Partnern (z.B. IBM) zuhören. Anschließend laden wir zu einem köstlichen Bankett mit kostenlosen Speisen und Getränken.
                    </p>

                    <?php foreach(DataProvider::$calCategories as $category):?>
                        <h3><?php echo DataProvider::getCategoryName($category);?></h3>
                        <table>
                            <?php foreach(DataProvider::loadCalendar($category) as $cal):?>
                                <tr>
                                    <td><?php echo $cal->time;?></td>
                                    <td>
                                        <h4><?php echo $cal->header;?></h4>
                                        <?php if(isset($cal->text)):?>
                                            <p><?php echo $cal->text;?></p>
                                        <?php endif;?>
                                        <?php if(isset($cal->speaker)):?>
                                            <p class="speaker"> <?php echo $cal->speaker;?></p>
                                        <?php endif;?>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        </table>
                    <?php endforeach;?>                    
                    
                </div>
                
                <div class="box"  id="zusagen">
                    <h2 id="zusagen">Zusagen</h2>
                    <p class="center">
                        Wenn Sie von uns eine Einladung per Mail oder Post erhalten haben, können Sie auch über diese Internetseite zusagen und das folgende Formular ausfüllen.
                    </p> 
                    <form action="" method="POST" id="register-form">
                        <fieldset>
                            <table class="form-table">
                                <tr>
                                    <td class="middle name-response">
                                        <label for="name" class="control-label">Vor- und Nachname</label>
                                    </td>
                                    <td colspan="2" class="name-response">
                                        <input type="text" class="form-control" name="name" value="" placeholder="Max Mustermann">
                                        <span class="help-block response-helpblock" id="name-helpblock"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="middle mail-response">
                                        <label for="mail" class="control-label">E-Mail-Adresse</label>
                                    </td>
                                    <td colspan="2" class="mail-response">
                                        <input type="email" class="form-control" name="mail" value="" placeholder="max@mustermail.de">
                                        <span class="help-block response-helpblock" id="mail-helpblock"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="checkbox-response">
                                        <label for="vortrag" class="control-label">Ich komme</label>
                                    </td>
                                    <td class="center checkbox-response table-footer">
                                        <input type="checkbox" class="form-control" name="vortrag" value="1" id="checkbox-vortrag"/>
                                        <p class="center help-block">
                                            Ich komme zu den Vorträgen
                                        </p>
                                    </td>
                                    <td class="center checkbox-response table-footer">
                                        <input type="checkbox" class="form-control" name="dinner" value="1" id="checkbox-dinner" disabled="disabled" />
                                        <p class="center">
                                            Ich komme auch zum Abendprogramm
                                        </p>
                                        <p class="center red tiny">
                                            Bitte haben Sie verständniss, dass wir die Teilnahme am Abendprogramm aufgrund der begrenzten Kapazität nur Gästen gestatten, die auch an den Vorträgen teilnehmen.
                                        </p>
                                        <span class="help-block response-helpblock" id="checkbox-helpblock"></span>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        
                        <button style="width: 100%;" type="submit" href="#" class="button" id="form-submit-btn">
                            Zur Veranstaltung zusagen
                        </button>
                    </form>
                </div>
                
            </div>
        
        </div><!-- content !-->
    </div><!-- wrapper !-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/script.js"></script>
    <script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
    <script>

        $(document).ready(function(){
           
           calibrate_everything();
           
           $(window).resize(function(){
               calibrate_everything();
           });
            
            $('.mask').mouseenter(function(){
                var win_w = $(window).width();
                if(win_w >= 920){
                    $(this).stop(true, false).animate({width: "43%"}, 300);
                    $(this).siblings().stop(true, false).animate({width: "28%"}, 300);
                }
            });
            
            $('.mask').mouseleave(function(){
                var win_w = $(window).width();
                if(win_w >= 920){
                    $(this).stop(true, false).animate({width: "33%"}, 300);
                    $(this).siblings().stop(true, false).animate({width: "33%"}, 300);
                }
            });
            
            $('h3.year').click(function(){
                if($(this).hasClass('permanent-red')){
                    $(this).removeClass('permanent-red');
                    $(this).next('.fader').fadeOut();
                }else{
                    $(this).addClass('permanent-red');
                    $(this).next('.fader').fadeIn();
                    $('html, body').animate({
                        scrollTop: ($(this).offset().top - 150)
                    }, 500);
                }
            });

            $('#checkbox-vortrag').change(function() {
                $("#checkbox-dinner").prop('disabled', !this.checked);
                if (!this.checked) {
                    $("#checkbox-dinner").attr("checked", false);
                }
            });

            $('#register-form').submit(function() {
                $('#form-submit-btn').prop('disabled', true);
                $.post('form.php', $('#register-form').serialize())
                .done(function(response) {
                    $('#myModal').modal('show');
                    $('#register-form').find('*').removeClass('has-error has-success has-warning');
                    $('#register-form').find('.response-helpblock').text('');      

                    $('#register-form').find('fieldset').prop('disabled', true);
                })
                .fail(function(response) {
                    $('#form-submit-btn').prop('disabled', false);
                    response = JSON.parse(response.responseText);

                    $('#register-form').find('*').removeClass('has-error has-success has-warning');
                    $('#register-form').find('.response-helpblock').text('');

                    if (response.errors.hasOwnProperty('name')) {
                        $('#register-form').find('.name-response').addClass('has-error');
                        $('#name-helpblock').text(response.errors.name);
                    }
                    if (response.errors.hasOwnProperty('mail')) {
                        $('#register-form').find('.mail-response').addClass('has-error');
                        $('#mail-helpblock').text(response.errors.mail);                        
                    }
                    if (response.errors.hasOwnProperty('checkboxes')) {
                        $('#register-form').find('.checkbox-response').addClass('has-error');
                        $('#checkbox-helpblock').text(response.errors.checkboxes);
                    }
                });
                return false;
            });       
            
            $('a.scroll-link').click(function(){
                var target = $( $.attr(this, 'href') );
                $('html, body').animate({
                    scrollTop: ($( $.attr(this, 'href') ).offset().top - 150)
                }, 500, function(){
                    $(target).css('background-color', '#faffae')
                    $(target).animate({
                        backgroundColor:"#fff"
                    },1000);
                });
                
                
                
                return false;
            });  

        });
    </script>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Schließen</span></button>
        <h4 class="modal-title" id="myModalLabel">Rückmeldung</h4>
      </div>
      <div class="modal-body">
        Sie haben sich erfolgreich angemeldet
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
      </div>
    </div>
  </div>
</div>   
        
<?php get_footer();?>
