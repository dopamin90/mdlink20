<?php
    
    require_once('functions.php');
    
    if(empty($_GET['page']) or !isset($_GET['page'])){
        load_page('home');
    }else{
        load_page($_GET['page']);
    }
    
?>